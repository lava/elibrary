<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%book_requests}}`.
 */
class m210323_075650_create_book_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%book_requests}}', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer(),
            'bookId' => $this->integer(),
            'resolution' => 'ENUM("allowed","denied","canceled")',
            'createdAt' => $this->datetime()->notNull(),
            'resolutionUpdatedAt' => $this->datetime()->notNull(),
        ]);

        $this->createIndex('{{%idx-resolution}}', '{{%book_requests}}', 'resolution');

        // creates index for column `userId`
        $this->createIndex(
            '{{%idx-book_requests-userId}}',
            '{{%book_requests}}',
            'userId'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-book_requests-userId}}',
            '{{%book_requests}}',
            'userId',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `bookId`
        $this->createIndex(
            '{{%idx-book_requests-bookId}}',
            '{{%book_requests}}',
            'bookId'
        );

        // add foreign key for table `{{%books}}`
        $this->addForeignKey(
            '{{%fk-book_requests-bookId}}',
            '{{%book_requests}}',
            'bookId',
            '{{%books}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-book_requests-userId}}',
            '{{%book_requests}}'
        );

        // drops index for column `userId`
        $this->dropIndex(
            '{{%idx-book_requests-userId}}',
            '{{%book_requests}}'
        );

        // drops foreign key for table `{{%books}}`
        $this->dropForeignKey(
            '{{%fk-book_requests-bookId}}',
            '{{%book_requests}}'
        );

        // drops index for column `bookId`
        $this->dropIndex(
            '{{%idx-book_requests-bookId}}',
            '{{%book_requests}}'
        );
        $this->dropTable('{{%book_requests}}');
    }
}
