<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_books}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%books}}`
 */
class m210323_104656_create_junction_user_and_books_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_books}}', [
            'userId' => $this->integer(),
            'bookId' => $this->integer(),
            'givenAt' => $this->dateTime()->notNull(),
            'PRIMARY KEY(userId, bookId)',
        ]);

        // creates index for column `userId`
        $this->createIndex(
            '{{%idx-user_books-userId}}',
            '{{%user_books}}',
            'userId'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_books-userId}}',
            '{{%user_books}}',
            'userId',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `bookId`
        $this->createIndex(
            '{{%idx-user_books-bookId}}',
            '{{%user_books}}',
            'bookId'
        );

        // add foreign key for table `{{%books}}`
        $this->addForeignKey(
            '{{%fk-user_books-bookId}}',
            '{{%user_books}}',
            'bookId',
            '{{%books}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_books-userId}}',
            '{{%user_books}}'
        );

        // drops index for column `userId`
        $this->dropIndex(
            '{{%idx-user_books-userId}}',
            '{{%user_books}}'
        );

        // drops foreign key for table `{{%books}}`
        $this->dropForeignKey(
            '{{%fk-user_books-bookId}}',
            '{{%user_books}}'
        );

        // drops index for column `bookId`
        $this->dropIndex(
            '{{%idx-user_books-bookId}}',
            '{{%user_books}}'
        );

        $this->dropTable('{{%user_books}}');
    }
}
