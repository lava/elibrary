<?php

namespace console\controllers;

use common\components\Rbac;
use common\models\User;
use Yii;
use yii\console\widgets\Table;
use yii\helpers\Console;

class AppController extends \yii\console\Controller
{
    use \common\traits\ConsoleWorkoutTrait;
    use \common\traits\LogTrait;

    public function actionInitRbac()
    {
        $accessSite = Rbac::createPermission(Rbac::P_ACCESS_SITE);
        $accessControlPanel = Rbac::createPermission(Rbac::P_ACCESS_CONTROL_PANEL);
        $accessPrivateCabinet = Rbac::createPermission(Rbac::P_ACCESS_PRIVATE_CABINET);

        $guest = Rbac::createRole(Rbac::R_GUEST, [$accessSite]);
        $user = Rbac::createRole(Rbac::R_USER, [$guest, $accessPrivateCabinet]);
        $administrator = Rbac::createRole(
            Rbac::R_ADMINISTRATOR,
            [$user, $accessControlPanel]
        );

        $user = new User();
        $user->username = 'admin';
        $user->email = 'admin@admin.local';
        $user->status = User::STATUS_ACTIVE;
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->setPassword('123');
        if (!$user->save()) {
            $this->dumpModel($user);
            $this->writeError('Failed to create default user "admin".');
        } else {
            if (!Rbac::assignRoleToUser($user, $administrator)) {
                $this->writeError('Failed to assign role to "admin".');
            }
        }

        $user = new User();
        $user->username = 'user';
        $user->email = 'user@user.local';
        $user->status = User::STATUS_ACTIVE;
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->setPassword('123');
        if (!$user->save()) {
            $this->dumpModel($user);
            $this->writeError('Failed to create default user "user".');
        } else {
            if (!Rbac::assignRoleToUser($user, $user)) {
                $this->writeError('Failed to assign role to "user".');
            }
        }
    }

    public function actionClearRbac()
    {
        Rbac::deleteAll();
    }
}
