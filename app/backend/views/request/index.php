<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BookRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Book Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-request-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'label' => 'User',
                'attribute' => 'userId',
                'format' => 'html',
                'value' => function ($model) {
                    return "#{$model->user->id} {$model->user->username}";
                }
            ],
            [
                'label' => 'Book',
                'attribute' => 'bookId',
                'format' => 'html',
                'value' => function ($model) {
                    return "#{$model->book->id} {$model->book->name}";
                }
            ],
            'resolution',
            'createdAt:datetime',
            //'resolutionUpdatedAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>