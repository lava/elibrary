<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BookRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'bookId')->textInput() ?>

    <?= $form->field($model, 'resolution')->dropDownList([ 'allowed' => 'Allowed', 'denied' => 'Denied', 'canceled' => 'Canceled', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'createdAt')->textInput() ?>

    <?= $form->field($model, 'resolutionUpdatedAt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
