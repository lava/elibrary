<?php

namespace common\components;

use Yii;
use yii\rbac\Role;
use yii\rbac\Permission;
use common\models\User;


class Rbac
{
    const P_ACCESS_SITE = 'accessSite';
    const P_ACCESS_PRIVATE_CABINET = 'accessPrivateCabinet';
    const P_ACCESS_CONTROL_PANEL = 'accessControlPanel';

    const R_GUEST = 'guest';
    const R_USER = 'user';
    const R_ADMINISTRATOR = 'administrator';

    /**
     * @param int|string|User $user User, users' id or username.
     * @param string|Role $role
     * @return boolean
     */
    public static function assignRoleToUser($user, $role): bool
    {
        $role = self::findRole($role);
        if (empty($role)) {
            Yii::error("Role not found.");
            return false;
        }
        $user = self::findUser($user);
        if (empty($user)) {
            Yii::error("User not found.");
            return false;
        }

        $roles = self::getUserRoles($user);
        // $roles = Yii::$app->authManager->getRolesByUser($user->id);
        if (array_key_exists($role->name, $roles)) {
            Yii::warning("Role '$role->name' already assigned to user $user->id:$user->phone");
            return true;
        }
        Yii::$app->authManager->assign($role, $user->id);
        return true;
    }




    /**
     * @param int|string|User $user User, users' id or username.
     * @param string|Role $role
     * @return boolean
     */
    public static function revokeUserRole($user, $role): bool
    {
        $role = self::findRole($role);
        if (empty($role)) {
            Yii::error("Role not found.");
            return false;
        }
        $user = self::findUser($user);
        if (empty($user)) {
            Yii::error("User not found.");
            return false;
        }

        return Yii::$app->authManager->revoke($role, $user->id);
    }


    public static function userHasRole($user, $roleName): bool
    {
        $roles = self::getUserRoles($user);
        return array_key_exists($roleName, $roles);
    }

    public static function userHasPermission($user, $permissionName): bool
    {
        $user = self::findUser($user);
        if (empty($user)) {
            Yii::error("User not found.");
            return false;
        }
        return Yii::$app->authManager->checkAccess($user->id, $permissionName);
    }


    public static function deleteAll()
    {
        Yii::$app->authManager->removeAll();
    }


    //roles

    /**
     * @param string $name
     * @param yii\rbac\Item[]|array $childItems can be array of Permission or Role or it's names
     * @param string $description
     * @return Role
     */
    public static function createRole(string $name, array $childItems = [], string $description = ''): Role
    {
        if (self::roleExists($name)) {
            Yii::warning("role '$name' already exists");
            return Yii::$app->authManager->getRole($name);
        }
        $role = Yii::$app->authManager->createRole($name);
        $role->description = $description;
        Yii::$app->authManager->add($role);
        if (!empty($childItems)) {
            self::addChildRoles($role, $childItems);
        }
        return $role;
    }

    /**
     * @param string|Role $role
     * @param Roles[]|string[] $childRoles
     * @return bool
     */
    public static function addChildRoles($role, array $childRoles): bool
    {
        if (!is_object($role)) {
            $role = self::getRole((string) $role);
        }
        if (empty($role)) {
            Yii::warning("empty role");
            return false;
        }
        $succeeded = true;
        foreach ($childRoles as $item) {
            if (is_string($item)) {
                $item = self::findRole($item);
            }
            if (!Yii::$app->authManager->addChild($role, $item)) {
                $succeeded = false;
            }
        }
        return $succeeded;
    }

    /**
     * @param string|Role $role
     * @param Permission[] $permissions
     * @return boolean
     */
    public static function assignPermissionsToRole($role, array $permissions)
    {
        if (!is_object($role)) {
            $role = self::getRole((string) $role);
        }
        if (empty($role)) {
            Yii::warning("empty role");
            return false;
        }
        foreach ($permissions as $item) {
            if (!Yii::$app->authManager->addChild($role, $item)) {
                return false;
            }
        }
        return true;
    }


    /**
     * @param string $roleName
     * @return boolean
     */
    public static function roleExists(string $roleName): bool
    {
        return self::getRole($roleName) !== null;
    }


    /**
     * @param string $roleName
     * @return Permission|null
     */
    public static function getRole(string $roleName)
    {
        return Yii::$app->authManager->getRole($roleName);
    }


    /**
     * @param int|string|User $user
     * @return Role[]
     */
    public static function getUserRoles($user): array
    {
        $user = self::findUser($user);
        if (empty($user)) {
            Yii::error("User not found.");
            return [];
        }

        return Yii::$app->authManager->getRolesByUser($user->id);
    }


    public static function deleteRole(string $roleName): bool
    {
        $item = self::getRole($roleName);
        if (empty($item)) {
            Yii::warning("empty role");
            return false;
        }
        return Yii::$app->authManager->remove($item);
    }



    //permissions

    /**
     * @param string $name
     * @param string $description
     * @param yii\rbac\Permission[] $childPermissions
     * @return Permission
     */
    public static function createPermission(string $name, array $childPermissions = [], string $description = ''): Permission
    {
        if (self::permissionExists($name)) {
            Yii::warning("permission '$name' already exists");
            return Yii::$app->authManager->getPermission($name);
        }
        $permission = Yii::$app->authManager->createPermission($name);
        $permission->description = $description;
        Yii::$app->authManager->add($permission);
        if (!empty($childPermissions)) {
            self::addChildPermissions($permission, $childPermissions);
        }
        return $permission;
    }

    /**
     * @param string|Permission $permission Permission or permission's name
     * @param yii\rbac\Permission[] $childPermissions
     * @return bool
     */
    public static function addChildPermissions($permission, array $childPermissions): bool
    {
        if (!is_object($permission)) {
            $permission = self::getPermission((string) $permission);
        }
        if (empty($permission)) {
            Yii::warning("empty permission");
            return false;
        }
        $succeeded = true;
        foreach ($childPermissions as $item) {
            if (!Yii::$app->authManager->addChild($permission, $item)) {
                $succeeded = false;
            }
        }
        return $succeeded;
    }

    /**
     * @param string $permissionName
     * @return boolean
     */
    public static function permissionExists(string $permissionName): bool
    {
        return self::getPermission($permissionName) !== null;
    }

    /**
     * @param string $permissionName
     * @return Permission|null
     */
    public static function getPermission(string $permissionName)
    {
        return Yii::$app->authManager->getPermission($permissionName);
    }


    public static function deletePermission(string $permissionName): bool
    {
        $item = self::getPermission($permissionName);
        if (empty($item)) {
            Yii::warning("empty permission");
            return false;
        }
        return Yii::$app->authManager->remove($item);
    }


    /**
     * Find user by id or username
     * @param int|string|User $user
     * @return User|null
     */
    private static function findUser($user)
    {
        if (is_int($user)) {
            $user = User::findOne($user);
        } else if (is_string($user)) {
            $user = User::findByUsername($user);
        }
        return $user;
    }

    private static function findRole($role)
    {
        if (!is_object($role)) {
            return self::getRole((string) $role);
        }
        return $role;
    }
}
