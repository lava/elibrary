<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%books}}".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $author
 * 
 * @property BookRequest[] $requests
 * @property UserBook[] $userBooksJunction
 * @property User[] $users
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%books}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'author'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'author' => 'Author',
        ];
    }

    /**
     * Gets query for [[BookRequests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBookRequests()
    {
        return $this->hasMany(BookRequest::class, ['bookId' => 'id']);
    }

    /**
     * Gets query for [[UserBooks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserBooksJunction()
    {
        return $this->hasMany(UserBook::class, ['books_id' => 'id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('{{%user_books}}', ['books_id' => 'id']);
    }

    public function givenToTheUser($userId): bool
    {
        return UserBook::find()->where(['bookId' => $this->id, 'userId' => $userId])->exists();
    }

    public function requestInProcessForUser($userId): bool
    {
        return BookRequest::find()
            ->where(['bookId' => $this->id, 'userId' => $userId, 'resolution' => null])
            ->exists();
    }
}
