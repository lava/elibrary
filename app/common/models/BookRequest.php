<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%book_requests}}".
 *
 * @property int $id
 * @property int|null $userId
 * @property int|null $bookId
 * @property string|null $resolution
 * @property string $createdAt
 * @property string $resolutionUpdatedAt
 *
 * @property Book $book
 * @property User $user
 */
class BookRequest extends \yii\db\ActiveRecord
{
    use \faryshta\base\EnumTrait;

    const RESOLUTION_ALLOWED = 'allowed';
    const RESOLUTION_DENIED = 'denied';
    const RESOLUTION_CANCELED = 'canceled';

    public static function enums()
    {
        return [
            'resolution' => [
                self::RESOLUTION_ALLOWED => 'Allowed',
                self::RESOLUTION_DENIED => 'Denied',
                self::RESOLUTION_CANCELED => 'Canceled',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%book_requests}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['createdAt', 'default', 'value' => date('Y-m-d H:i:s')],
            ['resolutionUpdatedAt', 'default', 'value' => date('Y-m-d H:i:s')],
            [['userId', 'bookId'], 'integer'],
            [['resolution'], 'string'],
            [['createdAt', 'resolutionUpdatedAt'], 'required'],
            [['createdAt', 'resolutionUpdatedAt'], 'safe'],
            [['bookId'], 'exist', 'skipOnError' => true, 'targetClass' => Book::class, 'targetAttribute' => ['bookId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['userId' => 'id']],
            // ['resolution', \faryshta\validators\EnumValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'bookId' => 'Book ID',
            'resolution' => 'Resolution',
            'createdAt' => 'Created At',
            'resolutionUpdatedAt' => 'Resolution Updated At',
        ];
    }

    /**
     * Gets query for [[Book]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::class, ['id' => 'bookId']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'userId']);
    }
}
