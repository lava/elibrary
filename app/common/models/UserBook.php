<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_books}}".
 *
 * @property int $userId
 * @property int $bookId
 * @property string $givenAt
 *
 * @property Book $book
 * @property User $user
 */
class UserBook extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_books}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'bookId', 'givenAt'], 'required'],
            [['userId', 'bookId'], 'integer'],
            [['givenAt'], 'safe'],
            [['userId', 'bookId'], 'unique', 'targetAttribute' => ['userId', 'bookId']],
            [['bookId'], 'exist', 'skipOnError' => true, 'targetClass' => Book::className(), 'targetAttribute' => ['bookId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'User ID',
            'bookId' => 'Books ID',
            'givenAt' => 'Given At',
        ];
    }

    /**
     * Gets query for [[Books]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'bookId']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
