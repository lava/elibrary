<?php

namespace common\traits;

use Yii;

trait FlashMessageTrait
{
    public function successMessage(string $message)
    {
        Yii::$app->session->addFlash('success', $message);
    }

    public function warningMessage(string $message)
    {
        Yii::$app->session->addFlash('warning', $message);
    }

    public function errorMessage(string $message)
    {
        Yii::$app->session->addFlash('error', $message);
    }

    /**
     * @param int|string $code
     */
    public function internalErrorMessage($code = '')
    {
        if (is_int($code)) {
            $code = sprintf('%02d', $code);
        }
        $code = empty($code) ? "[00]" : "[$code]";
        $email = Yii::$app->params['supportEmail'] ?? '';
        $this->errorMessage(
            "Не удалось завершить операцию - произошла внутренняя ошибка сервера $code. "
                . 'Попробуйте повторить попозже. Если ошибка повторяется, то <a href="mailto:' . $email . '">сообщите нам об этом</a>.'
        );
    }

    /**
     * Copy errors from any Model to show them to user
     *
     * @param yii\base\Model $model
     * @return void
     */
    public function showModelErrorsToUser($model)
    {
        foreach ($model->errors as $attr => $errors) {
            foreach ($errors as $error) {
                $this->errorMessage("[$attr] " . $error);
            }
        }
    }
}
