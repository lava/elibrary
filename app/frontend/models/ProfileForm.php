<?php

namespace frontend\models;

use common\models\User;
use Yii;
use yii\base\Model;
use yii\base\UserException;

/**
 * ContactForm is the model behind the contact form.
 */
class ProfileForm extends Model
{
    use \common\traits\LogTrait;

    public $username = '';
    public $email = '';
    public $userId = 0;

    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Name',
            'email' => 'Email',
        ];
    }

    public function loadProfileFromUser($userId): bool
    {
        $user = User::findOne($userId);
        if (!$user) {
            return false;
        }
        $this->userId = $user->id;
        $this->username = $user->username;
        $this->email = $user->email;
        return true;
    }

    public function save(): bool
    {
        if (!$this->validate()) {
            $this->dumpModel($this);
            return false;
        }
        $user = User::findOne($this->userId);
        if (!$user) {
            Yii::error("User id=$this->userId not found");
            return false;
        }
        $user->username = $this->username;
        $user->email = $this->email;
        if (!$user->save()) {
            Yii::error("Failed to save User");
            $this->dumpModel($user);
            return false;
        }
        return true;
    }
}
