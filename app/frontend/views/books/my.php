<?php

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $provider */

use yii\bootstrap\ActiveForm;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = 'My';
?>

<p>
    <a href="<?= Url::to(['/books']) ?>">All books</a> | <a href="<?= Url::to(['show-my']) ?>">My books</a>
</p>

<?= GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        'name',
        'author',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{read} | {return}',
            'buttons' => [
                'read' => function ($url, $model) {
                    return Html::a('Read', ['read', 'id' => $model->id]);
                },
                'return' => function ($url, $model) {
                    return Html::a('Return to library', ['return', 'id' => $model->id]);
                },
            ],
        ],
    ],
]); ?>