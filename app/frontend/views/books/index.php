<?php

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $provider */

use yii\bootstrap\ActiveForm;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = 'All';
?>

<p>
    <a href="<?= Url::to(['/books']) ?>">All books</a> | <a href="<?= Url::to(['show-my']) ?>">My books</a>
</p>


<?= GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        'name',
        'author',
        // 'status',
        [
            'format' => 'html',
            'value' => function ($model) {
                /** @var common\models\Book $model */
                if ($model->givenToTheUser(Yii::$app->user->id)) {
                    return Html::a('Read', ['read', 'id' => $model->id]);
                } else if ($model->requestInProcessForUser(Yii::$app->user->id)) {
                    return 'Requested. Waiting answer.';
                } else {
                    return Html::a('Send request', ['send-request', 'id' => $model->id]);
                }
            }
        ],
    ],
]); ?>