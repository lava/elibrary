<?php

/** @var yii\web\View $this */
/** @var frontend\models\ProfileForm $model */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(['id' => 'profile-form']); ?>

<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
<?= $form->field($model, 'email')->textInput() ?>
<?= $form->field($model, 'userId')->hiddenInput()->label(false) ?>
<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
</div>

<?php ActiveForm::end(); ?>