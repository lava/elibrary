<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\UserException;
use yii\data\ActiveDataProvider;

use common\models\Book;
use common\models\BookRequest;

class BooksController extends Controller
{
    use \common\traits\LogTrait;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'actions' => ['send-request', 'read', 'show-my'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        $provider = new ActiveDataProvider([
            'query' => Book::find(),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);
        return $this->render('index', [
            'provider' => $provider
        ]);
    }

    public function actionShowMy()
    {
        $provider = new ActiveDataProvider([
            'query' => Book::find()
                ->alias('b')
                ->select('b.*')
                ->joinWith('bookRequests r')
                ->where(['r.userId' => Yii::$app->user->id, 'r.resolution' => BookRequest::RESOLUTION_ALLOWED]),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);
        return $this->render('my', [
            'provider' => $provider
        ]);
    }

    public function actionSendRequest($id)
    {
        $request = new BookRequest();
        $request->userId = Yii::$app->user->id;
        $request->bookId = $id;

        if (!$request->save()) {
            $this->dumpModel($request);
            throw new UserException('Failed to send request.');
        }
        Yii::$app->session->setFlash('success', 'Request sended.');
        return $this->redirect('index');
    }

    public function actionRead($id)
    {
        Yii::$app->session->setFlash('success', 'Read action.');
        return $this->goHome();
    }
}
